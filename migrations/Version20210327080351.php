<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210327080351 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE adhering (id INT AUTO_INCREMENT NOT NULL, last_name VARCHAR(255) NOT NULL, first_name VARCHAR(255) NOT NULL, address VARCHAR(255) DEFAULT NULL, city_code VARCHAR(255) DEFAULT NULL, mail VARCHAR(255) NOT NULL, phone VARCHAR(255) DEFAULT NULL, password VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE lend (id INT AUTO_INCREMENT NOT NULL, book_id INT NOT NULL, adhering_id INT NOT NULL, lending_date DATETIME NOT NULL, date_back DATETIME NOT NULL, date_real_back DATETIME DEFAULT NULL, INDEX IDX_AF097D0416A2B381 (book_id), INDEX IDX_AF097D04109E5459 (adhering_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE lend ADD CONSTRAINT FK_AF097D0416A2B381 FOREIGN KEY (book_id) REFERENCES book (id)');
        $this->addSql('ALTER TABLE lend ADD CONSTRAINT FK_AF097D04109E5459 FOREIGN KEY (adhering_id) REFERENCES adhering (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE lend DROP FOREIGN KEY FK_AF097D04109E5459');
        $this->addSql('DROP TABLE adhering');
        $this->addSql('DROP TABLE lend');
    }
}
