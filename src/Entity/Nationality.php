<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\NationalityRepository;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=NationalityRepository::class)
 * @ApiResource(
 *      attributes={
 *          "order"={
 *              "wording"="ASC"
 *          }
 *      }
 * )
 */
class Nationality
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     //* @Groups({"listAuthorFull","listAuthorSimple"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     //* @Groups({"listAuthorFull","listAuthorSimple"})
     */
    private $wording;

    /**
     * @ORM\OneToMany(targetEntity=Author::class, mappedBy="nationality")
     */
    private $authors;

    public function __construct()
    {
        $this->authors = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWording(): ?string
    {
        return $this->wording;
    }

    public function setWording(string $wording): self
    {
        $this->wording = $wording;

        return $this;
    }

    /**
     * @return Collection|Author[]
     */
    public function getAuthors(): Collection
    {
        return $this->authors;
    }

    public function addAuthor(Author $author): self
    {
        if (!$this->authors->contains($author)) {
            $this->authors[] = $author;
            $author->setNationality($this);
        }

        return $this;
    }

    public function removeAuthor(Author $author): self
    {
        if ($this->authors->removeElement($author)) {
            // set the owning side to null (unless already changed)
            if ($author->getNationality() === $this) {
                $author->setNationality(null);
            }
        }

        return $this;
    }
}
