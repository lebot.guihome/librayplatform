<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\LendRepository;
use ApiPlatform\Core\Annotation\ApiResource;

/**
 * @ORM\Entity(repositoryClass=LendRepository::class)
 * @ApiResource()
 */
class Lend
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $lendingDate;

    /**
     * @ORM\Column(type="datetime")
     */
    private $dateBack;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $dateRealBack;

    /**
     * @ORM\ManyToOne(targetEntity=Book::class, inversedBy="lends")
     * @ORM\JoinColumn(nullable=false)
     */
    private $book;

    /**
     * @ORM\ManyToOne(targetEntity=Adhering::class, inversedBy="lends")
     * @ORM\JoinColumn(nullable=false)
     */
    private $adhering;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLendingDate(): ?\DateTimeInterface
    {
        return $this->lendingDate;
    }

    public function setLendingDate(\DateTimeInterface $lendingDate): self
    {
        $this->lendingDate = $lendingDate;

        return $this;
    }

    public function getDateBack(): ?\DateTimeInterface
    {
        return $this->dateBack;
    }

    public function setDateBack(\DateTimeInterface $dateBack): self
    {
        $this->dateBack = $dateBack;

        return $this;
    }

    public function getDateRealBack(): ?\DateTimeInterface
    {
        return $this->dateRealBack;
    }

    public function setDateRealBack(?\DateTimeInterface $dateRealBack): self
    {
        $this->dateRealBack = $dateRealBack;

        return $this;
    }

    public function getBook(): ?Book
    {
        return $this->book;
    }

    public function setBook(?Book $book): self
    {
        $this->book = $book;

        return $this;
    }

    public function getAdhering(): ?Adhering
    {
        return $this->adhering;
    }

    public function setAdhering(?Adhering $adhering): self
    {
        $this->adhering = $adhering;

        return $this;
    }
}
