<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\AdheringRepository;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass=AdheringRepository::class)
 * @ApiResource()
 * @UniqueEntity(
 *      fields={"mail"},
 *      message="le champ {{ value }} doit être unique"
 * )
 */
class Adhering implements UserInterface
{
    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_MANAGER = 'ROLE_MANAGER';
    const ROLE_ADHERING = 'ROLE_ADHERING';
    const DEFAULT_ROLE = 'ROLE_ADHERING';
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $firstName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $cityCode;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $mail;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="simple_array", length=255, nullable=true)
     */
    private $roles;

    /**
     * @ORM\OneToMany(targetEntity=Lend::class, mappedBy="adhering")
     */
    private $lends;

    public function __construct()
    {
        $this->lends = new ArrayCollection();
        $this->roles = self::DEFAULT_ROLE;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getCityCode(): ?string
    {
        return $this->cityCode;
    }

    public function setCityCode(?string $cityCode): self
    {
        $this->cityCode = $cityCode;

        return $this;
    }

    public function getMail(): ?string
    {
        return $this->mail;
    }

    public function setMail(string $mail): self
    {
        $this->mail = $mail;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return Collection|Lend[]
     */
    public function getLends(): Collection
    {
        return $this->lends;
    }

    public function addLend(Lend $lend): self
    {
        if (!$this->lends->contains($lend)) {
            $this->lends[] = $lend;
            $lend->setAdhering($this);
        }

        return $this;
    }

    public function removeLend(Lend $lend): self
    {
        if ($this->lends->removeElement($lend)) {
            // set the owning side to null (unless already changed)
            if ($lend->getAdhering() === $this) {
                $lend->setAdhering(null);
            }
        }

        return $this;
    }
    
    public function getRoles(): array
    {
        return $this->roles;
    } 

    public function setRoles(array $roles): self
    {
        $this->roles = $roles;
        return $this;
    }   
    
    public function getSalt()
    {
        return null;
    }
    
    public function getUsername()
    {
        return $this->getMail();
    }
    
    public function eraseCredentials()
    {

    }
}

