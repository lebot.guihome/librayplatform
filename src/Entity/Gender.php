<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\GenderRepository;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Annotation\ApiSubresource;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;

// création de routes spécifiques en fonction de groups
//          itemOperations={
//            "get_simple"={
//                "method"="GET",
//                "path"="/genders/{id}/simple",
//                "normalization_context"={"groups"={"listGenderSimple"}}
//            },
//            "get_full"={
//                "method"="GET",
//                "path"="/genders/{id}/full",
//                "normalization_context"={"groups"={"listGenderFull"}}
//            }
//          },
//          collectionOperations={"get"},
/**
 * @ORM\Entity(repositoryClass=GenderRepository::class)
 * @ApiResource(
 *      attributes={
 *          "order"={
 *              "wording"="ASC"
 *          }
 *      }
 * )
 */
class Gender
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     //* @Groups({"listGenderSimple","listGenderFull"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     //* @Groups({"listGenderSimple","listGenderFull"})
     */
    private $wording;

    /**
     * @ORM\OneToMany(targetEntity=Book::class, mappedBy="gender")
     * @ApiSubresource
     //* @Groups({"listGenderFull"})
     */
    private $books;

    public function __construct()
    {
        $this->books = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getWording(): ?string
    {
        return $this->wording;
    }

    public function setWording(string $wording): self
    {
        $this->wording = $wording;

        return $this;
    }

    /**
     * @return Collection|Book[]
     */
    public function getBooks(): Collection
    {
        return $this->books;
    }

    public function addBook(Book $book): self
    {
        if (!$this->books->contains($book)) {
            $this->books[] = $book;
            $book->setGender($this);
        }

        return $this;
    }

    public function removeBook(Book $book): self
    {
        if ($this->books->removeElement($book)) {
            // set the owning side to null (unless already changed)
            if ($book->getGender() === $this) {
                $book->setGender(null);
            }
        }

        return $this;
    }
}
