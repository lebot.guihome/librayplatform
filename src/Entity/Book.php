<?php

namespace App\Entity;

use App\Entity\Author;
use App\Entity\Editor;
use App\Entity\Gender;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\BookRepository;
use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use Symfony\Component\Serializer\Annotation\Groups;
use ApiPlatform\Core\Serializer\Filter\PropertyFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\RangeFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;

/**
 * @ORM\Entity(repositoryClass=BookRepository::class)
 * @ApiResource(
 *      attributes={
 *          "order"={
 *              "title"="ASC",
 *              "price"="DESC"
 *          }
 *      },
 * ),
 * @ApiFilter(
 *      SearchFilter::class,
 *      properties={
 *          "title":"ipartial",
 *          "author":"exact",
 *      }
 * ),
 * @ApiFilter(
 *      RangeFilter::class,
 *      properties={
 *          "price"
 *      }
 * ),
 * @ApiFilter(
 *      OrderFilter::class,
 *      properties={
 *          "title",
 *          "price",
 *          "author.lastName",
 *      }
 * ),
 * @ApiFilter(
 *      PropertyFilter::class,
 *      arguments={
 *          "parameterName": "properties",
 *          "overrideDefaultProperties": false,
 *          "whitelist" = {
 *              "isbn",
 *              "title",
 *              "price",
 *          },
 *      }
 * )
 */
class Book
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     //* @Groups({"listAuthorFull"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     //* @Groups({"listAuthorFull"})
     */
    private $isbn;

    /**
     * @ORM\Column(type="string", length=255)
     //* @Groups({"listAuthorFull"})
     */
    private $title;

    /**
     * @ORM\Column(type="float", nullable=true)
     //* @Groups({"listAuthorFull"})
     */
    private $price;

    /**
     * @ORM\ManyToOne(targetEntity=Gender::class, inversedBy="books")
     * @ORM\JoinColumn(nullable=false)
     //* @Groups({"listAuthorFull"})
     */
    private $gender;

    /**
     * @ORM\ManyToOne(targetEntity=Editor::class, inversedBy="books")
     * @ORM\JoinColumn(nullable=false)
     //* @Groups({"listAuthorFull"})
     */
    private $editor;

    /**
     * @ORM\ManyToOne(targetEntity=Author::class, inversedBy="books")
     * @ORM\JoinColumn(nullable=false)
     */
    private $author;

    /**
     * @ORM\Column(type="integer", nullable=true)
     //* @Groups({"listAuthorFull"})
     */
    private $year;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     //* @Groups({"listAuthorFull"})
     */
    private $language;

    /**
     * @ORM\OneToMany(targetEntity=Lend::class, mappedBy="book")
     */
    private $lends;

    public function __construct()
    {
        $this->lends = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIsbn(): ?string
    {
        return $this->isbn;
    }

    public function setIsbn(string $isbn): self
    {
        $this->isbn = $isbn;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(?float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getGender(): ?Gender
    {
        return $this->gender;
    }

    public function setGender(?Gender $gender): self
    {
        $this->gender = $gender;

        return $this;
    }

    public function getEditor(): ?Editor
    {
        return $this->editor;
    }

    public function setEditor(?Editor $editor): self
    {
        $this->editor = $editor;

        return $this;
    }

    public function getAuthor(): ?Author
    {
        return $this->author;
    }

    public function setAuthor(?Author $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getYear(): ?int
    {
        return $this->year;
    }

    public function setYear(?int $year): self
    {
        $this->year = $year;

        return $this;
    }

    public function getLanguage(): ?string
    {
        return $this->language;
    }

    public function setLanguage(?string $language): self
    {
        $this->language = $language;

        return $this;
    }

    /**
     * @return Collection|Lend[]
     */
    public function getLends(): Collection
    {
        return $this->lends;
    }

    public function addLend(Lend $lend): self
    {
        if (!$this->lends->contains($lend)) {
            $this->lends[] = $lend;
            $lend->setBook($this);
        }

        return $this;
    }

    public function removeLend(Lend $lend): self
    {
        if ($this->lends->removeElement($lend)) {
            // set the owning side to null (unless already changed)
            if ($lend->getBook() === $this) {
                $lend->setBook(null);
            }
        }

        return $this;
    }
}
