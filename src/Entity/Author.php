<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use App\Repository\AuthorRepository;
use Doctrine\Common\Collections\Collection;
use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=AuthorRepository::class)
 * @ApiResource()
 * 
 */
class Author
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     //* @Groups({"listAuthorFull","listAuthorSimple"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     //* @Groups({"listAuthorFull","listAuthorSimple"})
     */
    private $lastName;

    /**
     * @ORM\Column(type="string", length=255)
     //* @Groups({"listAuthorFull","listAuthorSimple"})
     */
    private $firstName;

    /**
     * @ORM\ManyToOne(targetEntity=Nationality::class, inversedBy="authors")
     * @ORM\JoinColumn(nullable=false)
     //* @Groups({"listAuthorFull","listAuthorSimple"})
     */
    private $nationality;

    /**
     * @ORM\OneToMany(targetEntity=Book::class, mappedBy="author")
     //* @Groups({"listAuthorFull"})
     */
    private $books;

    public function __construct()
    {
        $this->books = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getNationality(): ?Nationality
    {
        return $this->nationality;
    }

    public function setNationality(?Nationality $nationality): self
    {
        $this->nationality = $nationality;

        return $this;
    }

    /**
     * @return Collection|Book[]
     */
    public function getBooks(): Collection
    {
        return $this->books;
    }

    public function addBook(Book $book): self
    {
        if (!$this->books->contains($book)) {
            $this->books[] = $book;
            $book->setAuthor($this);
        }

        return $this;
    }

    public function removeBook(Book $book): self
    {
        if ($this->books->removeElement($book)) {
            // set the owning side to null (unless already changed)
            if ($book->getAuthor() === $this) {
                $book->setAuthor(null);
            }
        }

        return $this;
    }
}
