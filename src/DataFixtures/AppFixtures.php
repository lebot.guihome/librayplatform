<?php

namespace App\DataFixtures;

use App\Entity\Adhering;
use App\Entity\Book;
use App\Entity\Lend;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $manager;
    private $faker;
    private $repoBook;
    private $encodePassword;
    public function __construct(UserPasswordEncoderInterface $encodePassword)
    {
        $this->faker = Factory::create("fr_FR");
        $this->encodePassword = $encodePassword;
    }
    
        
    
    public function load(ObjectManager $manager)
    {
        $this->manager = $manager; 
        $this->repoBook = $this->manager->getRepository(Book::class);      
        $this->loadAdhering();
        $this->loadlend();
        $manager->flush();
    }

    public function loadAdhering()
    {
        $commune = [
            "78003", "78005", "78006", "78007", "78009", "78010", "78013", "78015", "78020", "78029",
            "78030", "78031", "78033", "78034", "78036", "78043", "78048", "78049", "78050", "78053", "78057",
            "78062", "78068", "78070", "78071", "78072", "78073", "78076", "78077", "78082", "78084", "78087",
            "78089", "78090", "78092", "78096", "78104", "78107", "78108", "78113", "78117", "78118"
        ];
        for($i=0; $i<25; $i++){
            $adhering = new Adhering();
            $adhering->setLastName($this->faker->lastName())
                     ->setFirstName($this->faker->firstName())
                     ->setAddress($this->faker->streetAddress())
                     ->setCityCode($commune[mt_rand(0, sizeof($commune)-1)])
                     ->setPhone($this->faker->phoneNumber())
                     ->setMail($this->faker->email())
                     ->setPassword($this->encodePassword->encodePassword($adhering,$adhering->getLastName()));
            $this->addReference("adhering".$i,$adhering);
            $this->manager->persist($adhering);
        }
        $adhering = new Adhering();
        $adhering->setLastName("home")
                 ->setFirstName("guihome")
                 ->setMail("guihome@free.fr")
                 ->setPassword($this->encodePassword->encodePassword($adhering, $adhering->getLastName()));
        $this->manager->persist($adhering);
        $this->manager->flush();
    }

    public function loadLend()
    {
        for($i=0; $i<25; $i++){
            $max = mt_rand(1,5);
            for($j=0; $j<=$max; $j++){
                $lend = new Lend();
                $book = $this->repoBook->find(1,49);
                $lend ->setBook($book)
                      ->setAdhering($this->getReference("adhering".$i))
                      ->setLendingDate($this->faker->dateTimeBetween('-6 months'));
                $dateBack = date('Y-m-d H:m:n',strtotime('15 days',$lend->getLendingDate()->getTimestamp()));
                $dateBack = \DateTime::createFromFormat('Y-m-d H:m:n', $dateBack);
                      $lend->setDateBack($dateBack);
                    if(mt_rand(1,3) ===1){
                        $lend->setDateRealBack($this->faker->dateTimeInInterval($lend->getLendingDate(),"+30 days"));
                    }
            $this->manager->persist($lend);
            }
        }
        $this->manager->flush();
    }
}
