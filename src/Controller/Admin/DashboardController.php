<?php

namespace App\Controller\Admin;

use App\Entity\Author;
use App\Entity\Book;
use App\Entity\Editor;
use App\Entity\Gender;
use App\Entity\Nationality;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        return parent::index();
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('LibraryPlatform');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Auteur', 'fas fa-list', Author::class);
        yield MenuItem::linkToCrud('Livre', 'fas fa-list', Book::class);
        yield MenuItem::linkToCrud('Editeur', 'fas fa-list', Editor::class);
        yield MenuItem::linkToCrud('Genre', 'fas fa-list', Gender::class);
        yield MenuItem::linkToCrud('Nationalité', 'fas fa-list', Nationality::class);
    }
}
