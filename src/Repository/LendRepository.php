<?php

namespace App\Repository;

use App\Entity\Lend;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Lend|null find($id, $lockMode = null, $lockVersion = null)
 * @method Lend|null findOneBy(array $criteria, array $orderBy = null)
 * @method Lend[]    findAll()
 * @method Lend[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LendRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Lend::class);
    }

    // /**
    //  * @return Lend[] Returns an array of Lend objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Lend
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
