<?php

namespace App\Repository;

use App\Entity\Adhering;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Adhering|null find($id, $lockMode = null, $lockVersion = null)
 * @method Adhering|null findOneBy(array $criteria, array $orderBy = null)
 * @method Adhering[]    findAll()
 * @method Adhering[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdheringRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Adhering::class);
    }

    // /**
    //  * @return Adhering[] Returns an array of Adhering objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Adhering
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
